import React, { Component } from 'react';
import { Layout, Row } from 'antd';
import MyHeader from './views/Header';
import MyFooter from './views/Footer';
import Blog from './views/Blog';

const { Content } = Layout;

class App extends Component {
    render() {
        return (
            <Row>                
                <Layout style={{ minHeight: '100vh' }}>
                    <MyHeader />
                    <Content style={{ margin: '0 16px' }}>                
                        <Blog />
                    </Content>
                    <MyFooter />
                </Layout>
            </Row >
        );
    }
}

export default App;