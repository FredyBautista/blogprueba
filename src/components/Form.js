import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Form, Input, Button } from 'antd';
import {db} from '../db/db';

const {TextArea} = Input;
const FormItem = Form.Item;
const makeField = Component => ({input, meta, children, ...rest}) => {
    const hasError = meta.touched && meta.invalid;
    return (
        <FormItem validateStatus={hasError ? "error" : "success"}>
            <Component {...input} {...rest} children={children} />
        </FormItem>
    );
}
const NewInput = makeField(Input);
const NewTextArea = makeField(TextArea);

class FormBlog extends React.Component {  
    ownHandleSubmit = form => {
        db.posts.put({...form}).then(() => console.log("success"));
    }

    render() {
        const {handleSubmit} = this.props;
        return (
            <Form onSubmit={handleSubmit(this.ownHandleSubmit)}>
                <Field name="title" component={NewInput} placeholder="Article Title" />
                <Field name="body" component={NewTextArea} placeholder="Article Body" />
                <Field name="author" component={NewInput} placeholder="Author" />
                <FormItem>
                    <Button type="primary" htmlType="submit">
                        Add Article
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const validate = values => {
    const errors = {};
    if(!values.title) {
        errors.title = "Required";
    }
    return errors;
}

export default reduxForm({form: 'FormBlog', validate})(FormBlog)