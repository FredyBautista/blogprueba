import React, {Component} from 'react';
import {Card} from 'antd';

const Post = (props) => {
    const {title, body, author} = props;
    return (
        <div>
            <Card title={title} >
                <p>{body}</p>
                <label>{author}</label>
            </Card>
        </div>
    )
}

export default Post;