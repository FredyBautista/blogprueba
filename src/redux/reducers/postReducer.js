const initialState = {
    posts: []
}
export const postReducer = (state = initialState, action) => { 
    switch(action.type){
        case "STARTED":
            return {
                ...state,
                loading: true
            };
        case "SUCCESS":
            return {
                ...state,
                loading: false,
                error: null,
                posts: [...state.posts, ...action.payload]
            }
        default:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
    }
}