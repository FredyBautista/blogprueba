import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import {postReducer} from './reducers/postReducer'

export const rootReducer = combineReducers({
    form: formReducer,
    posts: postReducer
});