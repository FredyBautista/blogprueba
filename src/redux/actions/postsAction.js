export const postAction = action => {
    return dispatch => {
        dispatch(addPostsStarted());
        action.then(res =>{
            dispatch(addPostsSuccess(res))
        })
        .catch(error => {
            dispatch(addPostsFailure(error))
        })
    }
}

const addPostsSuccess = posts => {
    return {
        type: "SUCCESS",
        payload: posts
    }
}

const addPostsStarted = () => ({
    type: "STARTED"
})

const addPostsFailure = error => ({
    type: "FAILURE",
    payload: {...error}
})