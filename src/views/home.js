import React, { Component } from 'react';
import Form from '../components/Form';
import {connect} from 'react-redux';
import {db} from '../db/db';
import {postAction} from '../redux/actions/postsAction';
import Post from '../components/Post';

class Home extends Component{
    componentDidMount(){
        this.props.setPosts(db.table('posts').toArray());
    }
    
    render(){
        const {posts} = this.props.posts;
        return (
            <div>
                <Form />
                {
                    posts.map(post => {                        
                        return (
                            <Post key={post.id} title={post.title} body={post.body} author={post.author} />
                        )
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
    setPosts: posts => dispatch(postAction(posts))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);