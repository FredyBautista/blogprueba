import React from 'react';
import { Layout, Icon, Row, Col } from 'antd';
const { Header } = Layout;

export const MyHeader = () => {
    return (
        <Header style={{ background: 'rgb(64, 64, 64)', padding: 0 }} >
            <Row gutter={16}>
                <Col span={22}>
                    <h1 style={{ textAlign:'center', color: 'white' }}> 
                        Blog
                    </h1> 
                </Col>
                <Col span={1}>
                    <Icon type="user" style={{fontSize:"30px", color:"white"}}/>
                </Col>

                <Col span={1}>
                    <Icon type="setting" style={{fontSize:"30px", color:"white"}}/>
                </Col>
            </Row>
        </Header>
    )
}

export default MyHeader;