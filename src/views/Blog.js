import React, { Component } from 'react';
import { Input, Button, Row, Col } from 'antd';
import Post from '../components/Post';

export class Blog extends Component {
    constructor(){
        super();
        this.state = {
            posts: [],
            comments: [],
            title: "",
            author: "",
            body: ""
        }
    }
    handleTitle = (e) => {//set the title
        this.setState({title: e.target.value});
    } 

    handleBody = e => {//set the body or description
        this.setState({body: e.target.value});
    }

    handleAuthor = e => {//set the author
        this.setState({author: e.target.value});
    }

    onClick = () => {//add to the state the new post
        const {posts, title, body, author} = this.state;
        let post = [{
            id: posts.length + 1,
            title: title,
            body: body,
            author: author
        }]
        this.setState(prevState => ({title: '', author: '', body: '', posts: prevState.posts.concat(post)}));
    }

    render(){
        return(
            <Row>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 2, offset: 2 }}></Col>
                <Col xs={{ span: 11, offset: 1 }} lg={{ span: 14, offset: 2 }}>
                    <Input placeholder="Title" onChange= {this.handleTitle} value={this.state.title}/>
                    <Input.TextArea placeholder="Body"  onChange= {this.handleBody} rows={4} value={this.state.body}/>
                    <Input placeholder="Author" onChange= {this.handleAuthor} value={this.state.author}/>
                    <Button type="primary" onClick={this.onClick}>Add Post</Button>
                    <br /><br /><br />
                    {
                        this.state.posts.map((post, index) => {
                            return (
                                <Post key={`post-${index}`} title={post.title} body={post.body} author={post.author}/>
                            )
                        }) 
                    }
                </Col>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 2, offset: 2 }}></Col>
            </Row>
        )
    }
}

export default Blog;