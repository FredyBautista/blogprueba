import React from 'react';
import { Layout } from 'antd';
const { Footer } = Layout;

export const MyFooter = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>
            Created by Fredy Bautista
        </Footer>
    )
}

export default MyFooter;